// Lesson 4 - Controls and Errors
// How to control flow and then to report errors

import Cocoa

// some globals

let dict = [
    "Vancouver" : "YYZ",
    "San Franciso" : "SFO",
    "Austin" : "AUS",
    "Peoria" : "PIA",
    "San Jose" : "SJC",
    "Orlando" : "MCO",
    "Chicago" : "ORD"
]

var cities = dict.map({ key, value in
    return key
})

cities.append("New York")

print(cities)

// for loop

for item in dict {
    
    if item.key == "Peoria" {
        // skip to the next
        continue
    }
    
    if item.key == "San Jose" {
        // break out of the loop
        break
    }
    
    // Note: this won't really be in any order
    
    print(item.value)
}

// case and while

var count = 0

while arc4random_uniform(8) < 7 {
    print("loop... \(count)")
    
    if count > (cities.count - 1) {
        print("Out of range")
        continue
    }
    
    switch cities[count] {
    case "Austin" :
        print("BBQ and Music")
    case "Vancouver" :
        print("Mountains and MacDevOps")
    case "Peoria" :
        print("Not much")
    default :
        print("An airport")
    }
    
    count += 1
}

// if let

if let jfk = dict["New York"] {
    print("New York has many airports: \(jfk)")
} else {
    // can't use jfk as a variable
    print("Can't find JFK")
}

// guard

for city in cities {
    print(city)
    guard let current = dict[city] else { continue }
    print(current)
}

// throw errors

// first an error set

enum cityErrors: Error {
    case noCity
    case noAirport
    case otherError
}

// now a function that throws

func returnCode(city: String ) throws -> String {
    
    guard let current = dict[city] else {
        throw cityErrors.noCity
    }
    
    return current
}

// and a do/catch block to catch and handle the error

do {
    print(try returnCode(city: "Austin"))
    print(try returnCode(city: "Miami"))
} catch {
    print("Error finding the city")
}

// Exercises

// 1. Create an if/if else/else statement
// 2. Do a compound if statement using && and/or || for and and or
// 3. use the optional try to "cheat" on do/catch using a default value for good measure











// Answers

// Exercise 1

let foo = "bar"

if foo == "test" {
    print("first option")
} else if foo == "something" {
    print("second option")
} else {
    print("fall through")
}

// Exercise 2

let test = false

if foo == "bar" && test {
    print("made it")
}

if foo != "bar" || !test {
    print("all negative here")
}

// Exercise 3

print(try? returnCode(city: "Miami") )
