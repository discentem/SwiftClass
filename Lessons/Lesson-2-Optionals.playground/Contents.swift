// Lesson 2 - Optionals
// Best to get this over with now...

import Cocoa

// set up a variable without a value
// like a qubit... you can have three states instead of two

var test : Bool?

if test == true {
    print("It is something")
}

if test == false {
    print("It isn't something")
}

if test == nil {
    print("What is this crazy thing?")
}

// Strings too

var something : String?

// Native functions, and some others, can handle nils

print(something)

// optional chaining works too, so you won't crash with nil-aware functions

print(something?.count)

// however, you can break things by forcing the unwrap

let anotherThing = something //as! String

// remove the comment above and watch it crash as you try to unwrap a nil

print(anotherThing)

// your friend nil coallescing, allows things to be unwrapped quickly and safely by providing a default value

let somethingElse = something ?? "No Value"

// this is no longer optional

print(somethingElse)

// now for something a bit more useful

let dict = [
    "Vancouver" : "YYZ",
    "San Franciso" : "SFO",
    "Austin" : "AUS"
]

let cityYouNeverGoTo = "Peoria"

print(dict[cityYouNeverGoTo])

if dict[cityYouNeverGoTo] == nil {
    print("This city isn't in the database")
}

// and another way to do this

print(dict[cityYouNeverGoTo] ?? "This city isn't in the database")

// Rules to live by...

print("Coalescing is easier than crashing...")

// Exercises

// 1. Set a variable to nil, test if it's nil or not
// 2. Assign a value to that variable, then print it without it showing as an optional
// 3. Set some values, then retrieve them and print them using nil coalsecing to ensure you don't crash
